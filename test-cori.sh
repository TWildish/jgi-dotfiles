#!/bin/bash

rm logs/cori* logs/slurm.create.pers-bbf.* 2>/dev/null

#./test-environment-variables.sh | tee logs/cori.login.log >/dev/null

sbatch --constraint haswell --time 00:01:00 --qos=jgi -A fungalp --job-name dynamic-burstbuffer \
	--output logs/cori.slurm.dyn-bbf.log \
	--error logs/cori.slurm.dyn-bbf.err \
	--bbf="bbf-dynamic.conf" \
	./test-environment-variables.sh

text=`sbatch --constraint haswell --time 00:01:00 --qos=jgi -A fungalp --job-name create-persistent \
	--output logs/slurm.create.pers-bbf.log \
	--error logs/slurm.create.pers-bbf.err \
	--bbf="bbf-persistent-create.conf" \
	./test-environment-variables.sh`
echo $text
jobid=`echo $text | awk '{ print $NF }'`

text=`sbatch --constraint haswell --time 00:01:00 --qos=jgi -A fungalp --job-name use-persistent \
	--output logs/cori.slurm.pers-bbf.log \
	--error logs/cori.slurm.pers-bbf.err \
	--bbf="bbf-persistent-use.conf" \
	--dependency=afterany:$jobid \
	./test-environment-variables.sh`
echo $text
jobid=`echo $text | awk '{ print $NF }'`

sbatch --constraint haswell --time 00:01:00 --qos=jgi -A fungalp --job-name no-burstbuffer \
	--output logs/cori.slurm.no-bbf.log \
	--error logs/cori.slurm.no-bbf.err \
	--bbf="bbf-persistent-destroy.conf" \
	--dependency=afterany:$jobid \
	./test-environment-variables.sh
