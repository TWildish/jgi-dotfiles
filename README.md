These login scripts help set up the NERSC environment for JGI users

See https://docs.google.com/document/d/1wGAYGre5dwuZ9v28vD4GTPG5HoMVNYTpb7oW2e2P1JU/edit?ts=5914e8d7 for the rationale behind this.

## Instructions ##

### Make sure your login environment is clean ###
First, make sure you are using the system dotfiles correctly. On any NERSC host:
```
> cd $HOME
> ls -l .bashrc .cshrc .login .profile .zprofile .zshenv .zshrc
```

You should see something like this, all those files should be links to **/global/homes/skel/read-only**:
```
lrwxrwxrwx 1 wildish wildish 36 Jun  1  2016 .bashrc -> /global/homes/skel/read-only/.bashrc
lrwxrwxrwx 1 wildish wildish 35 Jun  1  2016 .cshrc -> /global/homes/skel/read-only/.cshrc
lrwxrwxrwx 1 wildish wildish 35 Jun  1  2016 .login -> /global/homes/skel/read-only/.login
lrwxrwxrwx 1 wildish wildish 37 Jun  1  2016 .profile -> /global/homes/skel/read-only/.profile
lrwxrwxrwx 1 wildish wildish 38 Jun  1  2016 .zprofile -> /global/homes/skel/read-only/.zprofile
lrwxrwxrwx 1 wildish wildish 36 Jun  1  2016 .zshenv -> /global/homes/skel/read-only/.zshenv
lrwxrwxrwx 1 wildish wildish 35 Jun  1  2016 .zshrc -> /global/homes/skel/read-only/.zshrc
```

If you see something different, reset your dotfiles with the **fixdots** command (or by hand if you prefer). **fixdots** copies your original dotfiles to **$HOME/KeepDots.$DATE** and replaces them with the correct link.

Go through your **$HOME/.*.ext** files and **remove or comment out** anything that attempts to set **NERSC_HOST**. That is set by the system, and you don't need to mess with it.

While you're there, clean up (comment out or remove) anything else that sets the *TMPDIR or *SCRATCH variables, it'll confuse the issue.

### Update your dotfiles ###

Clone this repository:
```
git clone git@bitbucket.org:TWildish/jgi-dotfiles.git
```
Edit your .profile.ext and include this line near the top of the script:
```
. ~/jgi-dotfiles/bashrc.ext
```

### Test it ###
Log in in a separate window (or log out/back in again), and run the **test-environment-variables.sh** script from the **jgi-dotfiles** repository you just cloned. Check the results are as you expect, under all circumstances.

If you find a bug, please report it by updating the Google doc with full details. Feel free to fork the repository, fix the bug in your fork, and make a pull request, so I can update this repository easily.

Please also feel free to contribute scripts to test the various cases (Cori vs. Genepool, login vs. batch, burst-buffer vs. vanilla etc), I'd appreciate that :-)

### Next steps ###
Once we agree that this code implements what we want I'll find a permanent home for it, and we can announce it to the JGI via the weekly news and sysops.

### Future updates ###
Any future JGI-specific environment settings can be addressed in the same manner: fork, test, pull-request, deploy. This is better than the NERSC-wide, non-versioned setup anyway.