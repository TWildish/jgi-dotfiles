#!/bin/bash

rm logs/genepool* 2>/dev/null

./test-environment-variables.sh | tee logs/genepool.login.log >/dev/null

qsub -pe pe_slots 1 \
	-l h_rt=00:01:00 \
	-cwd \
	-o logs/genepool.batch.log \
	-e logs/genepool.batch.err \
	-N test-environment \
	./test-environment-variables.sh
