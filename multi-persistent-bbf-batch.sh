#!/bin/bash
#SBATCH --constraint haswell
#SBATCH --time 00:01:00
#SBATCH --qos=jgi
#SBATCH --account fungalp
#SBATCH --output logs/multi-bbf.log
#SBATCH --error  logs/multi-bbf.err
#DW persistentdw name=TW_BB_1
#DW persistentdw name=TW_BB_2
#DW persistentdw name=TW_BB_3
#DW persistentdw name=TW_BB_4
#DW jobdw capacity=80GB access_mode=private type=scratch

#
# JGI-specific settings for *SCRATCH, *TMPDIR on Cori, Edison, Genepool and Denovo.
#

echo " "
echo "HOSTNAME = $HOSTNAME"
echo " "
echo "NERSC_HOST = $NERSC_HOST"
echo " "
echo "BSCRATCH = $BSCRATCH"
echo " SCRATCH = $SCRATCH"
echo "LSCRATCH = $LSCRATCH"
echo " "
echo "LTMPDIR = $LTMPDIR"
echo "RTMPDIR = $RTMPDIR"
echo " TMPDIR = $TMPDIR"
echo " "
if [ "$NERSC_HOST" == "cori" ]; then
  echo "Listing any burst-buffer reservations"
  env | grep DW_ | sort
fi

if [ ! -z "$SCRATCH" ]; then
  echo " "
  echo "Space on SCRATCH ($SCRATCH):"
  df -h $SCRATCH
fi

if [ ! -z "$LSCRATCH" ]; then
  echo " "
  echo "Space on LSCRATCH ($LSCRATCH):"
  df -h $LSCRATCH
fi

if [ ! -z "$TMPDIR" ]; then
  echo " "
  echo "Space on TMPDIR ($TMPDIR):"
  df -h $TMPDIR
fi

if [ ! -z "$LTMPDIR" ]; then
  echo " "
  echo "Space on LTMPDIR ($LTMPDIR):"
  df -h $LTMPDIR
fi

if [ ! -z "$RTMPDIR" ]; then
  echo " "
  echo "Space on RTMPDIR ($RTMPDIR):"
  df -h $RTMPDIR
fi

echo ' '
echo ' '
df -h
